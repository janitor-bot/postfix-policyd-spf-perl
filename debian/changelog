postfix-policyd-spf-perl (2.011-2) UNRELEASED; urgency=medium

  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Fix day-of-week for changelog entry 2.004-1.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 12 Apr 2020 17:56:36 +0000

postfix-policyd-spf-perl (2.011-1) unstable; urgency=medium

  * New upstream release (Closes: #900512, #902801)
    - Refreshed patch
    - Update debian/copyright
    - Update debian/postfix-policyd-spf-perl.1 for upstream changes
  * Add Vcs-* for sals.d.o
  * Add mention of the more complete Python implementation in the long
    description, since it covers many more use cases (Closes: 865441)
  * Bump compat and debhelper version to 9
  * Switch to source format 3.0 (quilt)
  * Set priority to optional (was extra)
  * Update debian/watch to use secure URI and get rid of uupdate, no
    longer needed
  * Bump standards-version to 4.1.5 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sun, 29 Jul 2018 16:07:13 -0400

postfix-policyd-spf-perl (2.010-2) unstable; urgency=medium

  * Fix maintainer-script-should-not-use-adduser-system-without-home
  * Add mention of RFC 7208 to package description and man page
  * Bump standards version to 3.9.7 without further change

 -- Scott Kitterman <scott@kitterman.com>  Thu, 04 Feb 2016 17:36:27 -0500

postfix-policyd-spf-perl (2.010-1) unstable; urgency=low

  * New upstream release
    - Updated debian/patches/use-debian-mailname to work with new upstream use
      of libsys-hostname-long-perl
    - Added depends on libsys-hostname-long-perl
    - Updated debian/copyright

 -- Scott Kitterman <scott@kitterman.com>  Sun, 17 Jun 2012 23:55:06 -0400

postfix-policyd-spf-perl (2.009-2) unstable; urgency=low

  * Fix debian watch to work with Launchpad again
  * Update standards version to 3.9.3 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 19 Mar 2012 17:24:49 -0400

postfix-policyd-spf-perl (2.009-1) unstable; urgency=low

  * New upstream release
    - Refreshed patches

 -- Scott Kitterman <scott@kitterman.com>  Fri, 03 Feb 2012 23:31:43 -0500

postfix-policyd-spf-perl (2.008-2) unstable; urgency=low

  * Correct time limit parameter name in debian/postfix-policyd-spf-perl.1
    (Closes: #618282)

 -- Scott Kitterman <scott@kitterman.com>  Fri, 20 Jan 2012 00:17:14 -0500

postfix-policyd-spf-perl (2.008-1) unstable; urgency=low

  * New upstream release
    - Update debian/copyright
    - Add version requirement >= 2.006 for libmail-spf-perl
    - Update debian/postfix-policyd-spf-perl.1 man page
  * Fix long description to point to the non-dead location for the SPF web
    site
  * Overhaul debian/rules based on dh 7 tiny
  * Add debian/postfix-policyd-spf-perl.manpages and .install
  * Bump mimimum debhelper version to 7 and compat to 7
  * Add quilt to build-depends, build --with quilt, and add README.source
  * Add debian/patches/use-debian-mailname to read hostname from /etc/mailname
      - Thanks to Thomas Bullinger for the patch
  * Bump standards version to 3.9.2 without further change
  * Remove XS-DM-Upload-Allowed flag

 -- Scott Kitterman <scott@kitterman.com>  Thu, 19 Jan 2012 14:07:01 -0500

postfix-policyd-spf-perl (2.007-2) unstable; urgency=low

  * Update debian/control and debian/watch for new project location
  * Improve package description
  * Add ${misc:Depends} for postfix-policyd-spf-perl
  * Use the © symbol in debian/copyright
  * Bump standards version to 3.8.3 without further change

 -- Scott Kitterman <scott@kitterman.com>  Thu, 07 Jan 2010 14:34:19 -0500

postfix-policyd-spf-perl (2.007-1) unstable; urgency=low

  * New upstream release
    - Improved recommendations in documentation
  * Updated man postfix-policyd-spf-perl.1 based on upstream updates
    (Closes: #492421)
  * Create and use dedicated user instead of nobody (Closes: #492420)
    - Add postfix-policyd-spf-perl.postinst to create user if needed
    - Add depends on adduser
    - Update examples in man postfix-policyd-spf-perl.1 to use the dedicated
      user

 -- Scott Kitterman <scott@kitterman.com>  Fri, 25 Jul 2008 22:46:16 -0400

postfix-policyd-spf-perl (2.006-1) unstable; urgency=low

  * New upstream release
    - Enhanced logging by default
  * Update standards version to 3.8.0.1 without further change
  * Update debian/copyright
  * Change priority to extra due to depends
  * Minor updates to man page for correctness

 -- Scott Kitterman <scott@kitterman.com>  Fri, 18 Jul 2008 01:02:55 -0400

postfix-policyd-spf-perl (2.005-2) unstable; urgency=low

  * Update postfix-policyd-spf-perl(1) to better describe the packages
    capability to whitelist forwarders and secondary MXs from SPF checks
    (Closes: #468388)- patch thanks to Tim Small <tim@buttersideup.com> - and
    minor edits to quieten lintian
  * Change priority to optional to match over-ride
  * Minor updates to package short and long description to improve correctness

 -- Scott Kitterman <scott@kitterman.com>  Sun, 09 Mar 2008 21:20:41 -0400

postfix-policyd-spf-perl (2.005-1) unstable; urgency=low

  [ Scott Kitterman ]
  * New upstream release
    - Reduce DNS timeout period from 20 seconds to 10 seconds for faster
      performance with broken DNS servers
  * Move postfix from recommends to depends
  * Add watch file
  * Correct debian/copyright to only refer to GPL V2 and trim excess license
    quoting
  * Added Homepage field
  * Update standards version to 3.7.3 without further change
  * Clarify package description
  * Minor updates to postfix-policyd-spf-perl and move from section 8 to
    section 1
  * Minor white space cleanup in debian/control

  [ Philipp Kern ]
  * Added `XS-DM-Upload-Allowed: yes' to `debian/control' to allow uploads
    from Debian Maintainers.

 -- Scott Kitterman <scott@kitterman.com>  Sat, 15 Dec 2007 20:32:55 +0100

postfix-policyd-spf-perl (2.004-1) unstable; urgency=low

  * Initial Debian package (Closes: #236701)

 -- Scott Kitterman <scott@kitterman.com>  Wed, 18 Apr 2007 15:45:00 -0400
